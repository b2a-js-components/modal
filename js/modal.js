var modal = (function () {

    return {
        close() {

            let bodyEls = $(document.getElementsByTagName("body"));
            bodyEls.children(".overlay").remove();
            bodyEls.children(".modal").remove();
        },
        initialize: function (content) {
            let bodyEls = document.getElementsByTagName("body");
            
            for (let i=0;i<bodyEls.length; i++){
                let bodyEl = bodyEls[i];
                let overlay = $('<div class="overlay"></div>' );
                let modal = $('  <div class="modal"></div>');

                if (typeof content === 'function') {
                    content = content();
                }
                
                if (typeof content === 'string' || content instanceof String) {
                    modal.text(content);
                }
                else {
                    modal.append(content);
                }
                
                
                $(bodyEl).append(overlay);
                $(bodyEl).append(modal);
                
                overlay.click(() => {
                    overlay.remove();
                    modal.remove();
                });
            }
        }
    };
})();
